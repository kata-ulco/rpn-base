<?php

declare(strict_types=1);

namespace Ulco\Tests;

use Ulco\Rpn;
use PHPUnit\Framework\TestCase;

class RpnTest extends TestCase
{
    private ?Rpn $rpn = null;

    protected function setUp(): void
    {
        parent::setUp();

        $this->rpn = new Rpn();
    }
}